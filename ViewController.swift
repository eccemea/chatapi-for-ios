//
//  ViewController.swift
//  ChatAPI
//
//  Created by Daniel Szlaski on 07/05/2019.
//  Copyright © 2019 Daniel Szlaski. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView
import Starscream
import SwiftyJSON
import MaterialComponents.MaterialButtons




class ViewController: MessagesViewController {

    var messages: [Message] = []
    var customer, agent: Member!
    let waitSpinner = SpinnerViewController()
    
    let lblStatus = UILabel()
    
    // TODO: change var into let after tests are finished
    let organizationId = UserDefaults.standard.object(forKey: "organizationId") as! String? ?? String()  // organizationId
    let deploymentId = UserDefaults.standard.object(forKey: "deploymentId") as! String? ?? String()
    let queueName = UserDefaults.standard.object(forKey: "queueName") as! String? ?? String()
    let region = UserDefaults.standard.object(forKey: "region") as! String? ?? String()
    let firstName = UserDefaults.standard.object(forKey: "firstName") as! String? ?? String()
    let lastName = UserDefaults.standard.object(forKey: "lastName") as! String? ?? String()
    
    
    
    
    var jwt = ""
    var chatId = ""
    var eventStreamUri = ""
    var pc_conversationId = ""
    var pc_memberId = ""
    
    var socket = WebSocket(url: URL(string: "ws://")!, protocols: ["chat","superchat"])
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Test section
        /*
        organizationId = "942e91ad-65c1-4482-a793-a3fbcd46bb1d"
        deploymentId = "570c5509-9483-4f72-acc9-45455d80fa29"
        queueName = "Sales"
        region = "mypurecloud.com"
        
        */
        
        
        /* Starting from iOS 13, modal can be closed by swipe down gesture. Button not needed
         
        let btnClose = MDCFloatingButton()
        //button.setImage(plusImage, for: .normal)
        btnClose.setTitle("X", for: .normal)
        btnClose.setTitleColor(.white, for: .normal)
        btnClose.frame = CGRect(x: self.view.frame.size.width - 20 * 2, y: 30, width: 34, height: 34)
        
        self.view.addSubview(btnClose)
        btnClose.addTarget(self, action: #selector(closeChatWindow), for: .touchUpInside)
        
        */
        
        customer = Member(name: firstName + " " + lastName, color: .clear)
        agent = Member(name: "Agent", color: .clear)
                
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messageInputBar.delegate = self
        messagesCollectionView.messagesDisplayDelegate = self
    
        
        lblStatus.frame = CGRect(x: self.view.frame.size.width/2 - 90, y: 70, width: 180, height: 20)
        lblStatus.textAlignment = NSTextAlignment.center
        lblStatus.font = lblStatus.font.withSize(10)
        lblStatus.textColor = UIColor.lightGray
        
        lblStatus.text = "initializing chat session"
        
        self.view.addSubview(lblStatus)
        
        
        startChatSession()
        
      
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.attributedTextMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.attributedTextMessageSizeCalculator.incomingAvatarSize = .zero
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
            layout.photoMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.photoMessageSizeCalculator.incomingAvatarSize = .zero
            layout.videoMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.videoMessageSizeCalculator.incomingAvatarSize = .zero
            layout.locationMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.locationMessageSizeCalculator.incomingAvatarSize = .zero
            layout.emojiMessageSizeCalculator.incomingAvatarSize = .zero
            layout.emojiMessageSizeCalculator.outgoingAvatarSize = .zero
            
            layout.setMessageIncomingMessageTopLabelAlignment(LabelAlignment.init(textAlignment: .left, textInsets: .init(top: 0, left: 20, bottom: 0, right: 57)))
            layout.setMessageOutgoingMessageTopLabelAlignment(LabelAlignment.init(textAlignment: .right, textInsets: .init(top: 0, left: 57, bottom: 0, right: 20)))
        }
        
    }
    
    @objc func closeChatWindow() {
        print("closeChatWindow")
        self.closeChatSession(_conversationId: self.pc_conversationId, _memberId: self.pc_memberId)
    }
  
    func alertmessage(_title: String, msg: String) {
          DispatchQueue.main.async {
        let alert = UIAlertController(title: _title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            switch action.style{
            case .default:
                print("default")
                self.dismiss(animated: true, completion: nil) // return no main page in case of Error
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func createSpinnerView() {
        // add the spinner view controller
        addChild(waitSpinner)
        waitSpinner.view.frame = view.frame
        view.addSubview(waitSpinner.view)
        waitSpinner.didMove(toParent: self)
    }
    
    func hideSpinner() {
        DispatchQueue.main.async {
            self.waitSpinner.willMove(toParent: nil)
            self.waitSpinner.view.removeFromSuperview()
            self.waitSpinner.removeFromParent()
        }
    }
    
    
    func startChatSession() {
        // Send POST
        // prepare json data
        
        createSpinnerView()
        let json: [String: Any] = ["organizationId": organizationId,
                                   "deploymentId": deploymentId,
                                   "routingTarget": [
                                    "targetType": "QUEUE",
                                    "targetAddress": queueName
            ],
                                   "memberInfo": [
                                    "displayName": firstName + " " + lastName,
                                    "profileImageUrl": "http://amaovs.xp3.biz/img/photo/sample-image.jpg",
                                    "customFields": [
                                        "firstName": firstName,
                                        "lastName": lastName
                                    ]
            ]
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://api." + region + "/api/v2/webchat/guest/conversations")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                self.hideSpinner()
                self.alertmessage(_title: "Error", msg: error?.localizedDescription ?? "No data")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? NSDictionary
               
                let responseStatus = jsonResponse?["status"] as? Int
                if (responseStatus != nil && responseStatus != 200) {
                    self.hideSpinner()
                    self.alertmessage(_title: "Error", msg: jsonResponse?["message"] as! String)
                    return
                }
        
                
                self.jwt = jsonResponse?["jwt"] as! String
                self.chatId = jsonResponse?["id"] as! String
                self.eventStreamUri = jsonResponse?["eventStreamUri"] as! String
                
                print("Try to open socket connection")
                self.socket = WebSocket(url: URL(string: self.eventStreamUri)!, protocols: ["chat","superchat"])
                
                self.socket.disableSSLCertValidation = true
                self.socket.delegate = self
                self.socket.connect()
                print("wait...")
                
            }catch _ {
                print ("OOps not good JSON formatted response")
                self.hideSpinner()
                self.alertmessage(_title: "Error", msg: "General error in JSON response")
            }
        
           
        }
        
        task.resume()
    
    }
    
    
    func updateMemberName(_conversationId: String, _memberId: String) {
        
        print("updateMemberName for memberId " + _memberId)
        // create post request
        let url = URL(string: "https://api." + region + "/api/v2/webchat/guest/conversations/" + _conversationId + "/members/" + _memberId)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("bearer " + self.jwt, forHTTPHeaderField: "Authorization")
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
            
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                
                let responseStatus = jsonResponse?["status"] as? Int
                if (responseStatus != nil) {
                    return
                }
                
                // Update Agent
                self.agent.name = jsonResponse?["displayName"] as? String ?? "Agent"
               
                
            }catch _ {
                print ("OOps not good JSON formatted response")
            }
            
            
        }
        task.resume()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
             self.closeChatSession(_conversationId: self.pc_conversationId, _memberId: self.pc_memberId)
    }
    
    
    func closeChatSession(_conversationId: String, _memberId: String) {
        
        print("closeChatSession")
        // close socket first
        self.socket.disconnect()
        
        // create post request
        let url = URL(string: "https://api." + region + "/api/v2/webchat/guest/conversations/" + _conversationId + "/members/" + _memberId)!
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("bearer " + self.jwt, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        task.resume()
    }
    

}



// MARK: - WebSocketDelegate
extension ViewController : WebSocketDelegate {
    
    func websocketDidConnect(socket: WebSocketClient) {
          print("websocket is connected")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
          print("websocket discconnected")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        do {
            if let dataFromString = text.data(using: .utf8, allowLossyConversion: false) {
                let json = try JSON(data: dataFromString)
                
                 if (json["topicName"] == "channel.metadata") {
                    return
                }
                // Hide spinner when first Message from Socket is returned
                self.hideSpinner()
                
                
                print(json)
                
                if (json["eventBody"]["bodyType"] == "member-join") {
                    self.updateMemberName(_conversationId: self.pc_conversationId, _memberId: json["eventBody"]["sender"]["id"].stringValue)
                }
                
                self.lblStatus.text = json["eventBody"]["member"]["state"].stringValue
                
                
                if (pc_conversationId == "") {
                    if json["eventBody"]["member"]["id"].string != nil  {
                        // Do something you want
                        print("save conversation data (only once per interaction)")
                        pc_conversationId = json["eventBody"]["conversation"]["id"].stringValue
                        pc_memberId =  json["eventBody"]["member"]["id"].stringValue
                        
                        print("conv: " + pc_conversationId)
                        print("pc_memberId: " + pc_memberId)
                        
                    }
                }
                
                if ((json["eventBody"]["bodyType"] == "standard" || json["eventBody"]["bodyType"] == "notice")  &&  json["metadata"]["type"] == "message" && json["eventBody"]["sender"]["id"].string != self.pc_memberId ) {
                    
                    print("new message! -> " + json["eventBody"]["body"].stringValue)
                    
                    let newMessage = Message(
                        member: agent,
                        text: json["eventBody"]["body"].stringValue,
                        messageId: UUID().uuidString)
                    
                    messages.append(newMessage)
                    messagesCollectionView.reloadData()
                    messagesCollectionView.scrollToBottom(animated: true)
                } else if (json["metadata"]["type"] == "member-change" && json["eventBody"]["member"]["id"].string == self.pc_memberId && json["eventBody"]["member"]["state"] == "DISCONNECTED" ) {
                    
                    /*
                    let newMessage = Message(
                        member: agent,
                        text: "[Agent disconnected]",
                        messageId: UUID().uuidString)
                    
                    messages.append(newMessage)
                    messagesCollectionView.reloadData()
                    messagesCollectionView.scrollToBottom(animated: true)
                    */
                    
                    self.alertmessage(_title: "Information", msg: "Chat disconnected")
                
                }
            }
           
            
        } catch _ {
            print ("OOps not good JSON formatted response")
             self.alertmessage(_title: "Error", msg: "General error in JSON response")
        }
        
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("got some data: \(data.count)")
    }
    
    
    
}

extension ViewController: MessagesDataSource {
   
    func currentSender() -> SenderType {
        
        return Sender(senderId: customer.name, displayName: customer.name)
    }
  
    
    func numberOfSections(
        in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    

    
    func messageForItem(
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) -> MessageType {
        
        return messages[indexPath.section] as MessageType
    }
    
    func messageTopLabelHeight(
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        
        return 24
    }
    
    func messageTopLabelAttributedText(
        for message: MessageType,
        at indexPath: IndexPath) -> NSAttributedString? {
        
        return NSAttributedString(
            string: message.sender.displayName,
            attributes: [.font: UIFont.systemFont(ofSize: 12)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let formatter = DateFormatter()
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
    func cellTopLabelAttributedText(for message: MessageType,
                                    at indexPath: IndexPath) -> NSAttributedString? {
        
        let name = message.sender.displayName
        return NSAttributedString(
            string: name,
            attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption1),
                .foregroundColor: UIColor(white: 0.3, alpha: 1)
            ]
        )
    }
    
    
}


extension ViewController: MessagesLayoutDelegate {
 
    func avatarSize(for message: MessageType, at indexPath: IndexPath,
                    in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        // 1
        return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        // 2
        return CGSize(width: 0, height: 8)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath,
                           with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        
        // 3
        return 0
    }
    
 
    
}




extension ViewController: MessagesDisplayDelegate {
    /*
    func configureAvatarView(
        _ avatarView: AvatarView,
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) {
        
        
        let message = messages[indexPath.section]
        let color = message.member.color
        avatarView.backgroundColor = color
        
    }
    */
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        //let dateString =  BlueBoxUtilities.getStringFromDate(date: message.sentDate,fromat: "HH:mm")//formatter.string(from: message.sentDate)//message.createdDate//formatter.string(from: message.sentDate)
        let dateString = "10:15"
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.alignment = NSTextAlignment.left
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2), NSAttributedString.Key.paragraphStyle:paragraphStyle])
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) -> Bool {
        
        return false
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath,
                      in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        
        
        //let _borderColor:UIColor = isFromCurrentSender(message: message) ? .orange: .clear
        //return .bubbleTailOutline(borderColor, corner, .curved)
        
        
       return .bubbleTail(corner, .curved)
    }
    
    
    
}


extension ViewController: InputBarAccessoryViewDelegate {
    
   func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
    
    /* no more used
    if (text == "#bye") {
        self.dismiss(animated: true, completion: nil)
        return
    }
    */
    
    let newMessage = Message(
        member: customer,
        text: text,
        messageId: UUID().uuidString)
    
    messages.append(newMessage)
    inputBar.inputTextView.text = ""
    messagesCollectionView.reloadData()
    messagesCollectionView.scrollToBottom(animated: true)
    
    // #Send to PureCloud
    if (pc_conversationId != "") {
    
        // Send POST
        // prepare json data
        let json: [String: Any] = [
            "body": text,
            "bodyType": "standard"
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: "https://api." + region + "/api/v2/webchat/guest/conversations/" + pc_conversationId + "/members/" + pc_memberId + "/messages")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("bearer " + self.jwt, forHTTPHeaderField: "Authorization")
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                self.alertmessage(_title: "Error", msg: error?.localizedDescription ?? "No data")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            
        }
        
        task.resume()
    }
   
}
}




class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

