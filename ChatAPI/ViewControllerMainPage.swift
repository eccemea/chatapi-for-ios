//
//  ViewControllerMainPage.swift
//  ChatAPI
//
//  Created by Daniel Szlaski on 09/05/2019.
//  Copyright © 2019 Daniel Szlaski. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import MaterialComponents.MaterialButtons



class ViewControllerMainPage: UIViewController {

    
    @IBOutlet weak var button: MDCButton!
    @IBOutlet weak var txtOrganizationId: UITextField!    
    @IBOutlet weak var txtDeploymentId: UITextField!
    @IBOutlet weak var txtQueueName: UITextField!
    @IBOutlet weak var txtRegion: UITextField!
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblVersion: UILabel!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    
        // #organizationId
        let organizationId = UserDefaults.standard.object(forKey: "organizationId") as! String? ?? String()
        
        if (organizationId.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtOrganizationId.text = organizationId
        } else {
            txtOrganizationId.text = "5049461e-9547-41be-82ed-17f4961e9f1d" // PureCloud Poland
        }
        
        // #deploymentId
        let deploymentId = UserDefaults.standard.object(forKey: "deploymentId") as! String? ?? String()
        
        if (deploymentId.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtDeploymentId.text = deploymentId
        } else {
            txtDeploymentId.text = "c33e85a2-7be8-4d31-8a35-f3b43998f6ca" // PureCloud Poland
        }
        
        // #queueName
        let queueName = UserDefaults.standard.object(forKey: "queueName") as! String? ?? String()
        
        if (queueName.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtQueueName.text = queueName
        } else {
            txtQueueName.text = "Apple"
        }
        
        // #region
        let region = UserDefaults.standard.object(forKey: "region") as! String? ?? String()
        
        if (region.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtRegion.text = region
        } else {
            txtRegion.text = "mypurecloud.ie"
        }
        
        // #firstName
        let firstName = UserDefaults.standard.object(forKey: "firstName") as! String? ?? String()
        
        if (firstName.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtFirstName.text = firstName
        } else {
            txtFirstName.text = "Jon"
        }
        
        // #lastName
        let lastName = UserDefaults.standard.object(forKey: "lastName") as! String? ?? String()
        
        if (lastName.lengthOfBytes(using: String.Encoding.ascii) > 0) {
            txtLastName.text = lastName
        } else {
            txtLastName.text = "Snow"
        }
        
                
        
        let appVersionMajor = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
        let appVersionMinor = (Bundle.main.infoDictionary!["CFBundleVersion"] as? String)!
        print("v" + appVersionMajor + "(" + appVersionMinor + ")")
        lblVersion.text = "v" + appVersionMajor + "(" + appVersionMinor + ")"
        
      
        
    }
    
    
    @IBAction func btnStartChat(_ sender: Any) {

        print("save settings")
        let defaults = UserDefaults.standard
        defaults.set(txtOrganizationId.text , forKey: "organizationId")
        defaults.set(txtDeploymentId.text , forKey: "deploymentId")
        defaults.set(txtQueueName.text , forKey: "queueName")
        defaults.set(txtRegion.text , forKey: "region")
        defaults.set(txtFirstName.text , forKey: "firstName")
        defaults.set(txtLastName.text , forKey: "lastName")
        
        
        defaults.synchronize()
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with:event)
        self.view.endEditing(true)
    }
    
    
}


